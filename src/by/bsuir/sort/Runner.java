package by.bsuir.sort;

import by.bsuir.sort.algorithm.*;

import java.util.Arrays;
import java.util.stream.Stream;

public class Runner {

    private static final int[] data = {14, 43, -2, -18, -40, 51, 7, 13, 17, -6, 21, 3};
    //private static final int[] data = {14, 43, 2, 18, 40, 51, 7, 13, 17, 6, 21, 3};


    public static void main(String[] args) {

        Stream.of(
                new MinElementSort(),
                new ShellSort(),
                new QuickSort(),
                new BinaryTreeSearch(),
                new RadixSort(), //only for positive int
                new CombSort()).forEach(Runner::sort);
    }

    private static void sort(SortAlgorithm sortAlgorithm) {
        int[] array = Arrays.copyOf(data, data.length);

        System.out.println("Array:");
        System.out.println(Arrays.toString(array));

        sortAlgorithm.sort(array);

        System.out.println("Sorted array:");
        System.out.println(Arrays.toString(array));
        System.out.println("");
    }

}
