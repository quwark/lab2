package by.bsuir.sort.algorithm;

public interface SortAlgorithm {

    void sort(int[] array);
}
