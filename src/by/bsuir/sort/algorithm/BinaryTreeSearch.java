package by.bsuir.sort.algorithm;

public class BinaryTreeSearch implements SortAlgorithm {

    @Override
    public void sort(int[] array) {
        int n = array.length;
        for (int k = n / 2; k > 0; k--) downheap(array, k, n);do {
            int temp = array[0];
            array[0] = array[n - 1];
            array[n - 1] = temp;
            n = n - 1;
            downheap(array, 1, n);
        } while (n > 1);
    }

    private void downheap(int array[], int k, int n) {
        int T = array[k - 1];
        while (k <= n / 2) {
            int j = k + k;
            if ((j < n) && (array[j - 1] < array[j])) j++;
            if (T >= array[j - 1]) {
                break;
            } else {
                array[k - 1] = array[j - 1];
                k = j;
            }
        }
        array[k - 1] = T;
    }
}
