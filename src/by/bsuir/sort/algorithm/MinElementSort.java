package by.bsuir.sort.algorithm;

public class MinElementSort implements SortAlgorithm {

    public void sort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int least = i;
            for (int j = i + 1;j < arr.length ;j++) {
                if(arr[j] < arr[least]) {
                    least = j;
                }
            }
            int t = arr[i];
            arr[i] = arr[least];
            arr[least] = t;
        }
    }
}
