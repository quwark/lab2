package by.bsuir.sort.algorithm;


public class ShellSort implements SortAlgorithm {

    public void sort(int[] array) {
        int step = array.length / 2;
        while(step > 0){
            for(int i = 0; i < (array.length - step); i++) {
                for (int j = i; j >= 0 && array[j] > array[j + step]; j--) {
                    int t;
                    t = array[j + step];
                    array[j + step] = array[j];
                    array[j] = t;
                }
            }
            step = step / 2;
        }
    }
}
