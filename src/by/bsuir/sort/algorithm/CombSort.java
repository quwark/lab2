package by.bsuir.sort.algorithm;

public class CombSort implements SortAlgorithm {

    @Override
    public void sort(int[] array) {
        int gap = array.length;
        boolean swapped = true;
        while (gap > 1 || swapped) {
            if (gap > 1)
                gap = (int) (gap / 1.247330950103979);

            int i = 0;
            swapped = false;
            while (i + gap < array.length) {
                if (array[i] > (array[i + gap])) {
                    int temp = array[i];
                    array[i] = array[i + gap];
                    array[i + gap] = temp;
                    swapped = true;
                }
                i++;
            }
        }
    }


}
