package by.bsuir.sort.algorithm;


public class QuickSort implements SortAlgorithm {

    @Override
    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    private void sort(int[] array, int low, int high) {
        int i = low;
        int j = high;
        int x = array[low + (high - low) / 2];
        do {
            while(array[i] < x) {
                ++i;
            }
            while(array[j] > x) {
                --j;
            }
            if(i <= j) {
                swap(array, i, j);
                ++i;
                --j;
            }
        } while(i <= j);
        if(low < j) {
            sort(array, low, j);
        }
        if(i < high) {
            sort(array, i, high);
        }
    }
    private static void swap(int[] a, int i, int j){
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
